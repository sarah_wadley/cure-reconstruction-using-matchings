Sarah Wadley
COP4531 ~ progHW1

Project repo:
https://gitlab.com/sarah_wadley/cure-reconstruction-using-matchings

to compile this program use the command 'make'
from the same directory as the makefile and main.cpp

this will create 'main' which can be run with the input 
file like so:
    ./main < inputfile.txt

This program will output the correct order of vertices,
given the amount and each x and y position separated by comma



main.cpp contains the following functions:
  
  /*return distance between two points*/
  float distance(int x1, int y1, int x2, int y2)

  /*vertex object constructor*/
  vertex(const int ID, const int X, const int Y)

  /*populate and sort preference list by distance*/
  void assignDistance(vertex &v)

  /*return angle between 2 edges (3 vertices)*/
  double angle(vertex p1, vertex p2, vertex p3)

  /*assign neighboring edges*/
  void addEdge(std::vector<vertex> &v)

  /*accessors*/
  const int getID()
  const int getX()
  const int getY()
  const int getEdge1()
  const int getEdge2()

  /*assign an edge to every vertex in our list*/
  void compute_nn_edges(std::vector<vertex> &v)

  /*print final output for curveof vertex order*/
  void connectDots(std::vector<vertex> &v)
   
