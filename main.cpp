#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <cmath>
#include <algorithm>

#define PI 3.14159265

//return distance between two points
float distance(int x1, int y1, int x2, int y2){
    return std::sqrt(std::pow(x2 - x1, 2) + std::pow(y2 - y1, 2) * 1.0);
}

//~~~~~~~~~~~~~~~~~~class vertex~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//holds variables unique to each vertex
//and functions that operate on them
class vertex{
public:
  //vertex object constructor
  vertex(const int ID, const int X, const int Y){
    id = ID;
    x = X;
    y = Y;
    edge1 = -1;
    edge2 = -1;
  }

  //populate and sort preference list by distance
  void assignDistance(vertex &v)
  {
    preferences.push_back(std::make_pair(
          distance(x,y,v.getX(),v.getY()),
          v.getID()));
    std::sort(preferences.begin(), preferences.end());
  }

  //return angle between 2 edges (3 vertices)
  double angle(vertex p1, vertex p2, vertex p3)
  {
    //Angle = atan2 (Y - CenterY, X - CenterX)
    double result12 = atan2 (p1.getY()-p2.getY(),p1.getX()-p2.getX()) * 180 / PI;
    double result23 = atan2 (p3.getY()-p2.getY(),p3.getX()-p2.getX()) * 180 / PI;
    if(result12 < 0)
      result12 += 360;
    if(result23 < 0)
      result23 += 360;

    //return value is the angle between edges
    double angle = result12 - result23;
    if( angle < 0)
      angle *= -1;

    return angle;
  }

  //assign neighboring edges
  void addEdge(std::vector<vertex> &v)
  {
    int nextClosest = 1;
    //condition if vertex has no edges
    if(edge1 == -1)
      edge1 = preferences[nextClosest].second;
    //condition if vertex has 1 edge
    else
      //run error checking on angle
      while(edge2 == -1)
      {
        nextClosest++;
        //calculate angle between first edge and newly proposed edge
        if(angle(v[edge1],*this,v[preferences[nextClosest].second]) >= 90)
          edge2 = preferences[nextClosest].second;
      }
    return;
  }

  //accessors
  const int getID() const{
    return id;
  }
  const int getX() const{
    return x;
  }
  const int getY() const{
    return y;
  }
  const int getEdge1() const{
    return edge1;
  }
  const int getEdge2() const{
    return edge2;
  }

private:
  int id;
  int x;
  int y;
  int edge1;
  int edge2;
  std::vector< std::pair <float,int> > preferences;
};
//~~~~~~~~~~~~~~~~~~class vertex~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


//assign an edge to every vertex in our list
void compute_nn_edges(std::vector<vertex> &v)
{
  for(int i = 0; i < v.size(); i++)
    v[i].addEdge(v);
  return;
}

//print final output for curveof vertex order
void connectDots(std::vector<vertex> &v)
{
  std::set<int> usedPoints{0};
  int curr = 0;

  for(int i = 0; i < v.size(); i++){
    std::cout << v[curr].getID() << std::endl;

    usedPoints.insert(curr);
    //conditional runs if i is already in the set
    if (usedPoints.find(v[curr].getEdge1())==usedPoints.end())
      curr = v[curr].getEdge1();
    else
      curr = v[curr].getEdge2();
  }
  std::cout << v[0].getID() << std::endl;

  return;
}


//~~~~~~~~~~~~~~~~~~driver program~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/*this program will read in the number of vertices followed by their positions
  through standard input. Then it will output the points in the correct order
  to create a smooth curve with admissible edges. Assumes correct input*/
int main(){
  //declare variables to hold input
  std::string line;
  int numPoints;
  std::vector<vertex> v;

  //read in number of vertices
  std::getline (std::cin,line);
  numPoints = std::stoi(line);

  //assign each vertex to a vertex object
  char comma;
  int x, y;
  for (int z = 0; z < numPoints; z++)
  {
    std::cin >> x >> comma >> y;
    v.push_back(vertex(z,x,y));
  }

  //calculate preference list for each vertex
  for(int j = 0; j < numPoints; j++)
    for(int k = 0; k < numPoints; k++)
      v[j].assignDistance(v[k]);

  //assign first edge to each vertex
  compute_nn_edges(v);
  //assign second edge to each vertex
  compute_nn_edges(v);
  //display final curve sequence
  connectDots(v);

return 0;
}
//~~~~~~~~~~~~~~~~~~driver program~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
